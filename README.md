Buenas noches, adjunto nuevamente mi evaluación técnica de creación de una API ya que se encontraba inconvenientes al momento de compilar, habiendo corregido lo siguiente:
- Se borraron las carpetas del proyecto ya que en la descripción de ruta para direccionar se tenía que colocar la ruta de la carpeta mencionando mi nombre de usuario "vflores" de mi máquina, ya que de esa manera el GOPATH podría reconocer mi carpeta "controller" y "model".
- Para evitar otros inconvenientes cambié las sintaxis de código a un código puro, quiere decir que ya no consideré el uso del framework gingonic.
- Se cambió los nombres en el body de la respuesta.

SERVICIO REST:

METHOD: POST
ENDPOINT: localhost:8089/qds/test
DATA INPUT:
           {
             "array":[[1,2,3],[4,5,6],[7,8,9]]
           }

DATA OUTPUT:
           {
             "array": [[3,6,9],[2,5,8],[1,4,7]]
           }