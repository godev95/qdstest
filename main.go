package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/route"
)

func main() {

	route.SetRouter()

	server := http.ListenAndServe("0.0.0.0:8089", nil)
	fmt.Println("server running")
	log.Fatal(server)

}
