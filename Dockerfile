FROM golang:latest AS builder
RUN apt-get update
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
WORKDIR /qdstest
COPY go.mod .
COPY controller/go.mod .
RUN go mod download
COPY . .
RUN go build *.go
ENTRYPOINT ["./main"]
#FROM scratch
#COPY --from=builder /go/bin/app .
#ENTRYPOINT ["./main"]

#CMD ["./main"]
# docker build -t myapp . 
# dsudo s
#docker run --rm -it -p 8888:8888 myapp
