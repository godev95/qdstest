module qdstest

go 1.16

require (
	github.com/controller v0.0.0 // indirect
	github.com/route v0.0.0 //ct
)

replace (
	github.com/controller => ./controller
	github.com/helper => ./helper
	github.com/model => ./model
	github.com/route => ./route
)
