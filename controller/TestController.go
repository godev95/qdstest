package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//Input QDS
type Inputqds struct {
	Array [][]int `json:"array" binding:"required"`
}

type response struct {
	Array    [][]int `json:"array,omitempty"`
	Response string  `json:"error,omitempty"`
}

func Test(w http.ResponseWriter, r *http.Request) {

	//declarar la variable para el modelo de request y response
	var qds Inputqds

	//leer el body del request
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(w, "validación incorrecta")
		fmt.Println(err)
		return
	}

	//asociarlo al modelo input
	json.Unmarshal(reqBody, &qds)

	//determinar la variable para ver el total de valores que hay en el array
	totalr := len(qds.Array)

	//validar si en caso el arreglo este nulo
	if totalr == 0 {
		//se lanza una respuesta del servicio
		resp := &response{Response: "Array vacío"}

		body, err := json.Marshal(resp)
		if err != nil {
			fmt.Println(w, "validación incorrecta")
			fmt.Println(err)
			return
		}

		w.WriteHeader(http.StatusBadRequest)
		//se lanza una respuesta del servicio
		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		if _, err := w.Write(body); err != nil {
			fmt.Println(err)
			return
		}

		return
	}

	//creacion de la estructura del multiarray
	inverso := make([][]int, totalr)
	for x := 0; x < totalr; x++ {
		inverso[x] = make([]int, totalr)
	}

	var z int

	/*
		- Realizar la iteración para que el total de la matriz sea unida a la estructura multiarray
		creada en el paso anterior
	*/
	for i := 0; i < totalr; i++ {
		z = 0

		/*
			- El qds.Array me da los valores del primer arreglo dentro del array
			- El len me va a dar el total de valores que hay dentro de ese array
			- Inicializamos el bucle con la cantidad de esos valores menos 1
			- Se realiza el bucle en retroceso para capturar los ultimos valores de los arrays y con eso hacer la vuelta de 90 grados */
		for j := len(qds.Array[i]) - 1; j >= 0; j-- {
			//correr en retroceso

			/*
				- Si en caso es diferente quiere decir que el total de array del primer nivel
				es diferente al total de array del segundo nivel. Ejemplo
				[[1,2,3],[4,5,6]]: en este caso saldría error ya que el total del primer nivel que
				vendría ser 2 es diferente al total de valores del segundo nivel que es 3, por ende, no cumple con una matriz NxN
			*/
			if totalr != len(qds.Array[i]) {

				//se lanza una respuesta del servicio
				resp := &response{Response: "La longitud se debe respetar con modelo de matriz NxN"}

				body, err := json.Marshal(resp)
				if err != nil {
					fmt.Println(w, "validación incorrecta")
					fmt.Println(err)
					return
				}

				w.WriteHeader(http.StatusBadRequest)
				//se lanza una respuesta del servicio
				w.Header().Set("Content-Type", "application/json; charset=utf-8")

				if _, err := w.Write(body); err != nil {
					fmt.Println(err)
					return
				}

				return
			}
			//los ultimos valores de cada array se encapsula en un nuevo array, por ende, obtenemos asi una vuelta de 90 grados
			inverso[z][i] = qds.Array[i][j]
			z++
		}
	}

	resp := &response{Array: inverso}

	body, err := json.Marshal(resp)
	if err != nil {
		fmt.Println(w, "validación incorrecta")
		fmt.Println(err)
		return
	}

	//se lanza una respuesta del servicio
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if _, err := w.Write(body); err != nil {
		fmt.Println(err)
	}

}
